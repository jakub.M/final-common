import { Prop, Validator } from './validator.interface';
import { StringSchema, ObjectSchema, string, object } from 'yup';

export class Validation {
  private static schemaMap: Map<any, Prop[]> = new Map();
  private static formMap: Map<any, string> = new Map();

  static registerForm(target: any, path: string) {
    const formName = target.name;
    this.formMap.set(formName, path);
  }

  static getFormPath(metatype: any) {
    const formName = metatype.name;
    return this.formMap.get(formName) ?? '';
  }

  static registerCustomValidation(target: any, propertyKey: string, validator: StringSchema<string>) {
    const targetParams = this.getKeys(target);
    const index = targetParams.findIndex(({ name }) => name === propertyKey);
    if (index === -1) {
      targetParams.push({ name: propertyKey, validator });
    } else {
      targetParams[index].validator = validator;
    }
  }

  static registerValidation(target: any, propertyKey: string, validator?: Validator): void {
    const targetParams = this.getKeys(target);
      let index = targetParams.findIndex(({ name }) => name === propertyKey);
      if (index === -1) {
        targetParams.push({ name: propertyKey });
        index = targetParams.length - 1;
      }
      if (validator) {
        const params = targetParams[index]
        targetParams[index] = { ...params, validator: this.prepareValidator(validator, params.validator)}
      }
  }

  static prepareValidator({ validationFunction, args }: Validator, propSchemaValidation?: StringSchema<string>): StringSchema<string> {
    if (!propSchemaValidation) {
      propSchemaValidation = string();
    }
    return propSchemaValidation[validationFunction](...args);
  }

  static getKeys(target: any) {
    const keyName = target.constructor.name;
    let keys = this.schemaMap.get(keyName);
    if (!keys) {
      keys = [];
      this.schemaMap.set(keyName, keys);
    }
    return keys;
  }

  static getSchema(metatype: any): ObjectSchema<object | undefined> | undefined {
    const schemaProps = this.schemaMap.get(metatype.name);
    if (!schemaProps) {
      return undefined;
    }
    return object().shape(schemaProps.reduce((res, prop) => ({ ...res, [prop.name]: prop.validator }), {}));
  }

  static transform(value: any, metatype: any) {
    return this.schemaMap.get(metatype.name).reduce((res, { name }) => ({ ...res, [name]: value[name] }), {});
  }
}
