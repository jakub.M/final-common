import { StringSchema } from 'yup';

export interface Prop {
  name: string;
  validator?: StringSchema<string | undefined>;
}

export interface Validator {
  validationFunction: string;
  args: any[]
}
