import { Prop, Validator } from './validator.interface';
import { StringSchema, ObjectSchema } from 'yup';
export declare class Validation {
    private static schemaMap;
    private static formMap;
    static registerForm(target: any, path: string): void;
    static getFormPath(metatype: any): string;
    static registerCustomValidation(target: any, propertyKey: string, validator: StringSchema<string>): void;
    static registerValidation(target: any, propertyKey: string, validator?: Validator): void;
    static prepareValidator({ validationFunction, args }: Validator, propSchemaValidation?: StringSchema<string>): StringSchema<string>;
    static getKeys(target: any): Prop[];
    static getSchema(metatype: any): ObjectSchema<object | undefined> | undefined;
    static transform(value: any, metatype: any): {};
}
