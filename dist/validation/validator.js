"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Validation = void 0;
const yup_1 = require("yup");
class Validation {
    static registerForm(target, path) {
        const formName = target.name;
        this.formMap.set(formName, path);
    }
    static getFormPath(metatype) {
        var _a;
        const formName = metatype.name;
        return (_a = this.formMap.get(formName)) !== null && _a !== void 0 ? _a : '';
    }
    static registerCustomValidation(target, propertyKey, validator) {
        const targetParams = this.getKeys(target);
        const index = targetParams.findIndex(({ name }) => name === propertyKey);
        if (index === -1) {
            targetParams.push({ name: propertyKey, validator });
        }
        else {
            targetParams[index].validator = validator;
        }
    }
    static registerValidation(target, propertyKey, validator) {
        const targetParams = this.getKeys(target);
        let index = targetParams.findIndex(({ name }) => name === propertyKey);
        if (index === -1) {
            targetParams.push({ name: propertyKey });
            index = targetParams.length - 1;
        }
        if (validator) {
            const params = targetParams[index];
            targetParams[index] = Object.assign(Object.assign({}, params), { validator: this.prepareValidator(validator, params.validator) });
        }
    }
    static prepareValidator({ validationFunction, args }, propSchemaValidation) {
        if (!propSchemaValidation) {
            propSchemaValidation = yup_1.string();
        }
        return propSchemaValidation[validationFunction](...args);
    }
    static getKeys(target) {
        const keyName = target.constructor.name;
        let keys = this.schemaMap.get(keyName);
        if (!keys) {
            keys = [];
            this.schemaMap.set(keyName, keys);
        }
        return keys;
    }
    static getSchema(metatype) {
        const schemaProps = this.schemaMap.get(metatype.name);
        if (!schemaProps) {
            return undefined;
        }
        return yup_1.object().shape(schemaProps.reduce((res, prop) => (Object.assign(Object.assign({}, res), { [prop.name]: prop.validator })), {}));
    }
    static transform(value, metatype) {
        return this.schemaMap.get(metatype.name).reduce((res, { name }) => (Object.assign(Object.assign({}, res), { [name]: value[name] })), {});
    }
}
exports.Validation = Validation;
Validation.schemaMap = new Map();
Validation.formMap = new Map();
//# sourceMappingURL=validator.js.map