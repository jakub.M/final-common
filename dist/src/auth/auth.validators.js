"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoginDto = void 0;
const validation_1 = require("../../decorators/validation");
const pwd_decorator_1 = require("../../decorators/customs/pwd.decorator");
const email_decorator_1 = require("../../decorators/customs/email.decorator");
let LoginDto = class LoginDto {
};
__decorate([
    email_decorator_1.Email(),
    __metadata("design:type", String)
], LoginDto.prototype, "email", void 0);
__decorate([
    pwd_decorator_1.Pwd(),
    __metadata("design:type", String)
], LoginDto.prototype, "pwd", void 0);
LoginDto = __decorate([
    validation_1.Form('/auth/login')
], LoginDto);
exports.LoginDto = LoginDto;
//# sourceMappingURL=auth.validators.js.map