"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Email = void 0;
const validation_1 = require("../validation");
exports.Email = () => validation_1.combainDecorators(validation_1.IsRequired(), validation_1.IsEmail());
//# sourceMappingURL=email.decorator.js.map