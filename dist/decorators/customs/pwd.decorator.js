"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pwd = void 0;
const validation_1 = require("../validation");
exports.Pwd = () => validation_1.combainDecorators(validation_1.IsRequired(), validation_1.MinLength(8));
//# sourceMappingURL=pwd.decorator.js.map