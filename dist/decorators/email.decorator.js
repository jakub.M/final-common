"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Email = void 0;
const types_and_validatros_1 = require("types-and-validatros");
exports.Email = () => types_and_validatros_1.combainDecorators(types_and_validatros_1.IsRequired(), types_and_validatros_1.IsEmail());
//# sourceMappingURL=email.decorator.js.map