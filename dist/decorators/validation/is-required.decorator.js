"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsRequired = void 0;
const register_function_1 = require("./register-function");
exports.IsRequired = register_function_1.registerDecorator('required', 'Field is required!');
//# sourceMappingURL=is-required.decorator.js.map