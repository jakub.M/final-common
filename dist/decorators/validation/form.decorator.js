"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Form = void 0;
const validation_1 = require("../../validation");
exports.Form = (path) => (target) => validation_1.Validation.registerForm(target, path);
//# sourceMappingURL=form.decorator.js.map