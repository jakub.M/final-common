export declare const registerDecorator: <T extends any[]>(validationFunction: string, baseMsg: string) => (...args: T) => (target: any, propertyKey: string) => void;
