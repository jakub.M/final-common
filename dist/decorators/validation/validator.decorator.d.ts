import { StringSchema } from 'yup';
export declare const Validator: (data: StringSchema<string>) => (target: any, propertyKey: string) => void;
