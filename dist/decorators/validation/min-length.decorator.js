"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MinLength = void 0;
const register_function_1 = require("./register-function");
exports.MinLength = register_function_1.registerDecorator('min', 'Too short');
//# sourceMappingURL=min-length.decorator.js.map