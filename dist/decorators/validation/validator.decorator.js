"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Validator = void 0;
const validation_1 = require("../../validation");
exports.Validator = (data) => (target, propertyKey) => validation_1.Validation.registerCustomValidation(target, propertyKey, data);
//# sourceMappingURL=validator.decorator.js.map