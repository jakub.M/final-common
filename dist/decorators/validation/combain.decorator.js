"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.combainDecorators = void 0;
exports.combainDecorators = (...decorators) => (target, propertyKey) => {
    decorators.map((decorator) => decorator(target, propertyKey));
};
//# sourceMappingURL=combain.decorator.js.map