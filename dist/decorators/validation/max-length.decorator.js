"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MaxLength = void 0;
const register_function_1 = require("./register-function");
exports.MaxLength = register_function_1.registerDecorator('max', 'Too long!');
//# sourceMappingURL=max-length.decorator.js.map