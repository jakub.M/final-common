"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.registerDecorator = void 0;
const validation_1 = require("../../validation");
exports.registerDecorator = (validationFunction, baseMsg) => (...args) => (target, propertyKey) => validation_1.Validation.registerValidation(target, propertyKey, { validationFunction, args: [...args, baseMsg] });
//# sourceMappingURL=register-function.js.map