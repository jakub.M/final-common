"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsEmail = void 0;
const register_function_1 = require("./register-function");
exports.IsEmail = register_function_1.registerDecorator('email', 'Email is invalid!');
//# sourceMappingURL=is-email.decorator.js.map