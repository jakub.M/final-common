"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pwd = void 0;
const types_and_validatros_1 = require("types-and-validatros");
exports.Pwd = () => types_and_validatros_1.combainDecorators(types_and_validatros_1.IsRequired(), types_and_validatros_1.MinLength(8));
//# sourceMappingURL=pwd.decorator.js.map