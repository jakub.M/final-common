export interface LoginReq {
  email: string;
  pwd: string;
}

export interface FindUserResult extends LoginRes {
  id: string;
}
export interface LoginRes {
  email: string;
}
