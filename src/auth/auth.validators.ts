import { Form } from '../../decorators/validation';
import { Pwd } from '../../decorators/customs/pwd.decorator';
import { Email } from '../../decorators/customs/email.decorator';

@Form('/auth/login')
export class LoginDto {
  @Email()
  email: string;

  @Pwd()
  pwd: string;
}
