import { combainDecorators, IsRequired, MinLength } from '../validation';

export const Pwd = () => combainDecorators(IsRequired(), MinLength(8));
