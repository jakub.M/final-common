import { combainDecorators, IsRequired, IsEmail } from '../validation';

export const Email = () => combainDecorators(
  IsRequired(),
  IsEmail(),
);
