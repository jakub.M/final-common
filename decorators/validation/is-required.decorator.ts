import { registerDecorator } from './register-function';

export const IsRequired = registerDecorator('required', 'Field is required!');
