import { Validation } from '../../validation';

export const registerDecorator = <T extends Array<any>>(validationFunction: string, baseMsg: string) =>
    (...args: T) =>
      (target: any, propertyKey: string) =>
         Validation.registerValidation(target, propertyKey, { validationFunction, args: [...args, baseMsg]});
