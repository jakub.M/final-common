import { registerDecorator } from './register-function';

export const IsEmail = registerDecorator('email', 'Email is invalid!');
