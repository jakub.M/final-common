import { registerDecorator } from './register-function';

export const MaxLength = registerDecorator<[number]>('max', 'Too long!');
