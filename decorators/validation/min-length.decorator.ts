import { registerDecorator } from './register-function';

export const MinLength = registerDecorator<[number]>('min', 'Too short');
