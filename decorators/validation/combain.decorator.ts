export const combainDecorators = (...decorators: Function[]) => (target: any, propertyKey: string) => {
  decorators.map((decorator) => decorator(target, propertyKey));
};
