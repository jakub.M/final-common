import { Validation } from '../../validation';

export const Form = (path: string) => (target: any) =>
  Validation.registerForm(target, path);
