import { StringSchema } from 'yup';
import { Validation } from '../../validation';

export const Validator = (data: StringSchema<string>) => (target: any, propertyKey: string) =>
  Validation.registerCustomValidation(target, propertyKey, data);
